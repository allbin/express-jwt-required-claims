# @allbin/express-jwt-required-claims

```js
import express from 'express';
import jwt from 'express-jwt';
import claims_required from '@allbin/express-jwt-required-claims';

app = express();

app.get(
    '/protected',
    jwt({ secret: 'shhhhh' }),
    claims_required({
        uid: 123,
        access: { op: 'bits', value: 1 },
    }),
    (req, res, next) => {
        res.status(200);
    },
);

app.listen(8080);
```
