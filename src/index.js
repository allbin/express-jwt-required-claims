const satisfies = (claim, spec) => {
    if (typeof spec !== 'object') {
        return claim === spec;
    } else {
        if (spec.op && spec.op === 'bits') {
            return claim & (spec.value === spec.value);
        } else {
            return false;
        }
    }
    return false;
};

/* example options:
 *
 *  {
 *      "uid": 123,
 *      "access": { op: "bits", value: 1 }
 *  }
 */

export default options => {
    return (req, res, next) => {
        if (!req.user) {
            return res.status(403).json({ message: 'Missing token' });
        }

        let fail = Object.keys(options)
            .map(claim => [claim, options[claim]])
            .some(([claim, spec]) => !satisfies(claim, spec));

        if (fail) {
            return res
                .status(403)
                .json({ message: 'Missing a required claim' });
        }

        next();
    };
};
